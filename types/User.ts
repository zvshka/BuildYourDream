export interface User {
  bio: string;
  id: string;
  email: string;
  emailVerified?: any;
  avatarUrl?: string;
  username: string;
  role: string;
  isBanned: boolean;
  createdAt: any;
  updatedAt: any;
}
